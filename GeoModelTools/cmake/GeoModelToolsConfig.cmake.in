# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# First off, import the upstream dependencies of the project.
find_package( GeoModelCore @GeoModelCore_VERSION@ )
find_package( GeoModelIO @GeoModelIO_VERSION@ )
find_package( nlohmann_json @nlohmann_json_VERSION@ )
find_package( XercesC @XercesC_VERSION@ )

# Include the exported configuration of GeoModelCore.
get_filename_component( SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH )
include( ${SELF_DIR}/GeoModelToolsTargets.cmake )

# Set the version of the installed project.
set( GeoModelTools_VERSION "@PROJECT_VERSION@" )

# Print some standard messages about the package being found.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModelTools
   FOUND_VAR GeoModelTools_FOUND
   REQUIRED_VARS SELF_DIR
   VERSION_VAR GeoModelTools_VERSION )

# Clean up.
unset( SELF_DIR )

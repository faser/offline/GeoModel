# The GeoModel Toolkit - Public Papers and Notes



## Papers

- **GeoModel @ CHEP 2019**: *under review by the Abstracts' editors, coming soon*

- **GeoModel @ CHEP 2016**: Bianchi R M, Boudreau J, Vukotic I, 2017 J. Phys.: Conf. Ser.898 072015 <br /> <https://doi.org/10.1088/1742-6596/898/7/072015>

- **Original GeoModel paper @ CHEP 2004**: Boudreau J and Tsulaia V 2004 *"The GeoModel Toolkit for Detector Description"*, CHEP ’04, Book of Abstracts <br /> <https://indico.cern.ch/event/0/contributions/1294152/>


## Presentations

- **Original GeoModel presentation, at CHEP 2004**: Boudreau J and Tsulaia V 2004 *"The GeoModel Toolkit for Detector Description"*, CHEP ’04 <br /> <https://indico.cern.ch/event/0/contributions/1294152/>
